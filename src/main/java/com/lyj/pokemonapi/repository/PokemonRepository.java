package com.lyj.pokemonapi.repository;

import com.lyj.pokemonapi.entity.Pokemon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PokemonRepository extends JpaRepository<Pokemon, Long> {
}
