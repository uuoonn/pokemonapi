package com.lyj.pokemonapi.repository;

import com.lyj.pokemonapi.entity.BattleStage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BattleStageRepository extends JpaRepository<BattleStage, Long> {
}
