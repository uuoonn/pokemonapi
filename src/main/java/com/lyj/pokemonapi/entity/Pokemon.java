package com.lyj.pokemonapi.entity;

import com.lyj.pokemonapi.enums.Type;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity

public class Pokemon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String imageUrl;

    @Column(nullable = false)
    private String name;

    @Enumerated(value = EnumType.STRING )
    @Column(nullable = false)
    private Type type;

    @Column(nullable = false)
    private String character;

    @Column(nullable = false)
    private String classify;

    @Column(nullable = false)
    private String explain;

    @Column(nullable = false)
    private Integer monHp;

    @Column(nullable = false)
    private Integer monPower;

    @Column(nullable = false)
    private Integer monDefensive;

    @Column(nullable = false)
    private Integer monSpeed;


}
