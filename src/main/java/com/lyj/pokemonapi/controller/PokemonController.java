package com.lyj.pokemonapi.controller;

import com.lyj.pokemonapi.model.*;
import com.lyj.pokemonapi.model.monster.MonsterCreateRequest;
import com.lyj.pokemonapi.model.monster.MonsterResponse;
import com.lyj.pokemonapi.model.monster.PokemonItem;
import com.lyj.pokemonapi.service.PokemonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pokemon")

public class PokemonController {
    private final PokemonService pokemonService;

    @PostMapping("/create-monster")
    //setMonster를 CommonResult로 감싸서 등록시키고 공손하게 보여준다.
    public CommonResult setMonster(@RequestBody MonsterCreateRequest request) {
        pokemonService.setMonster(request);

        //새로운 빈 그릇 생성 후 보여줄 처리 결과 값 작성하기
        CommonResult result = new CommonResult();
        result.setMsg("몬스터 등록 완료");
        result.setCode(0);

        return result;

    }


    @GetMapping("/all")
    //ListResult(큰그릇)안에 PokemonItem리스트(작은그릇)를 넣어 보여줄거다.
    //하위 기능들을 하는 메서드의 이름은 getPokemons이다.
    public ListResult<PokemonItem> getPokemons() {

        //List<PokemonItem>의 새로운 이름은 list이다.
        //이 값은 pokemonService의 getPokemons 메서드의 빈그릇이다.
        List<PokemonItem> list = pokemonService.getPokemons();

        //ListResult(큰그릇)안에 PokemonItem리스트(작은그릇)를 넣은 것의 새로운 이름은 response.
        //이건 새로운 리스트 묶음이 된다.
        ListResult<PokemonItem> response = new ListResult<>();

        response.setList(list);
        response.setTotalCount(list.size());
        response.setCode(0);
        response.setMsg("성공하였습니다.");

        return response;
    }

    @GetMapping("/detail/{id}")
    //큰 그릇 SingleResult 안에 작은 그릇 MonsterResponse 값 넣어서 반환

    public SingleResult<MonsterResponse> getPokemon(@PathVariable long id) {

        //service의 getPokemon을 불러와 id값을 입력받아야한다. => result 그릇에 담기
        MonsterResponse result = pokemonService.getPokemon(id);

        //큰 그릇에 작은 그릇 넣은 것 새로운 빈 그릇 생성
        SingleResult<MonsterResponse> response = new SingleResult<>();

        response.setMsg("상세보기가 가능합니다.");
        response.setCode(0);
        //MonsterResponse의 내용을 service>getPokemon에서 가공해준 데이터를 넣는다.
        response.setData(result);

        return response;
    }



}
