package com.lyj.pokemonapi.controller;
import com.lyj.pokemonapi.entity.Pokemon;
import com.lyj.pokemonapi.model.CommonResult;
import com.lyj.pokemonapi.model.SingleResult;
import com.lyj.pokemonapi.model.battle.MonsterInBattleResponse;
import com.lyj.pokemonapi.service.BattleStageService;
import com.lyj.pokemonapi.service.PokemonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/battle")
public class BattleController {
    private final BattleStageService battleStageService;
    private final PokemonService pokemonService;

    @PostMapping("/join-id/{pokemonId}")
    public CommonResult setBattle(@PathVariable long pokemonId) throws Exception {
        Pokemon pokemon = pokemonService.getData(pokemonId);
        battleStageService.setBattle(pokemon);

        CommonResult result = new CommonResult();
        result.setMsg("입장성공!");
        result.setCode(0);

        return result;
    }

    @DeleteMapping("/del/{battleId}")
    public CommonResult delStageInMonster(@PathVariable long battleId) {
        battleStageService.delStageInMonster(battleId);

        CommonResult result = new CommonResult();
        result.setMsg("퇴장성공");
        result.setCode(0);

        return result;
    }

    @GetMapping("/in-battle-stage")
    public SingleResult<MonsterInBattleResponse> getCurrentState() {
        MonsterInBattleResponse result = battleStageService.getCurrentState();

        SingleResult<MonsterInBattleResponse> response = new SingleResult<>();
        response.setData(result);
        response.setMsg("입장");
        response.setCode(0);

        return response;
    }




}
