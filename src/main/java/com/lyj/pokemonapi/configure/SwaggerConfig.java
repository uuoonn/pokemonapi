package com.lyj.pokemonapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "pokemon",
                description = "pokemon api",
                version = "v1"))
@RequiredArgsConstructor
@Configuration

public class SwaggerConfig {

    @Bean
    public GroupedOpenApi pokemonOpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("포켓몬스타 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
