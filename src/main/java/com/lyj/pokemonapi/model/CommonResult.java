package com.lyj.pokemonapi.model;

import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class CommonResult {
    private String msg;
    private Integer code;
}
