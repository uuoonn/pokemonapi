package com.lyj.pokemonapi.model.monster;

import com.lyj.pokemonapi.enums.Type;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MonsterCreateRequest {
    private String imageUrl;

    private String name;

    @Enumerated(value = EnumType.STRING)
    private Type type;

    private String character;

    private String classify;

    private String explain;

    private Integer monHp;

    private Integer monPower;

    private Integer monDefensive;

    private Integer monSpeed;
}
