package com.lyj.pokemonapi.model.battle;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MonsterInBattleResponse {

    private MonsterInBattleItem monster1;

    private MonsterInBattleItem monster2;

}
