package com.lyj.pokemonapi.model.battle;

import com.lyj.pokemonapi.entity.Pokemon;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MonsterInBattleItem {

    private Long id;

    private Long monsterId;

    private String name;

    private String imageUrl;

    private Integer monHp;

    private Integer monPower;

    private Integer monDefensive;

    private Integer monSpeed;

}
