package com.lyj.pokemonapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public enum Type {
    GRASS("풀"),
    FLAME("불꽃"),
    WATER("물"),
    WORM("벌레"),
    FLIGHT("비행"),
    ELECTRIC("전기"),
    FAIRY("페어리");

    private final String type;

}
