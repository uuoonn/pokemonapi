package com.lyj.pokemonapi.service;

import com.lyj.pokemonapi.entity.Pokemon;
import com.lyj.pokemonapi.model.monster.MonsterCreateRequest;
import com.lyj.pokemonapi.model.monster.MonsterResponse;
import com.lyj.pokemonapi.model.monster.PokemonItem;
import com.lyj.pokemonapi.repository.PokemonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class PokemonService {
    private final PokemonRepository pokemonRepository;

    public Pokemon getData(long id) {
        return pokemonRepository.findById(id).orElseThrow();
    }

    public void setMonster(MonsterCreateRequest request) {
        //새로운 포켓몬스터를 등록할 수 있는 빈 그릇 생성
        Pokemon addData = new Pokemon();

        //addData라는 빈 그릇에 request 값을 넣어준다.
        addData.setImageUrl(request.getImageUrl());
        addData.setName(request.getName());
        addData.setType(request.getType());
        addData.setCharacter(request.getCharacter());
        addData.setClassify(request.getClassify());
        addData.setExplain(request.getExplain());
        addData.setMonHp(request.getMonHp());
        addData.setMonPower(request.getMonPower());
        addData.setMonDefensive(request.getMonDefensive());
        addData.setMonSpeed(request.getMonSpeed());

        //addData 값을 pokemonRepository 창고에 저장해준다.
        pokemonRepository.save(addData);


    }

    public List<PokemonItem> getPokemons() {
        List<Pokemon> orginList = pokemonRepository.findAll();

        List<PokemonItem> result = new LinkedList<>();
        for(Pokemon pokemon : orginList) {
            PokemonItem addItem = new PokemonItem();
            addItem.setId(pokemon.getId());
            addItem.setImageUrl(pokemon.getImageUrl());
            addItem.setName(pokemon.getName());
            addItem.setType(pokemon.getType());
            addItem.setCharacter(pokemon.getCharacter());

            result.add(addItem);
        }
        return result;
    }
    // 상세보기 서비스 구현 MonsterResponse
    // Pokemon id 필요하다.
    public MonsterResponse getPokemon(long id) {
        //Pokemon에서 id를 찾는다. 아니면 던진다.
        Pokemon pokemon = pokemonRepository.findById(id).orElseThrow();

        //MonsterResponse의 새로운 빈그릇 생성
        MonsterResponse response = new MonsterResponse();

        //MonsterResponse의 모든 내용 response 그릇에 넣고 데이터를 가공해준다.
        response.setId(pokemon.getId());
        response.setImageUrl(pokemon.getImageUrl());
        response.setName(pokemon.getName());
        response.setType(pokemon.getType());
        response.setCharacter(pokemon.getCharacter());
        response.setClassify(pokemon.getClassify());
        response.setExplain(pokemon.getExplain());
        response.setMonHp(pokemon.getMonHp());
        response.setMonPower(pokemon.getMonPower());
        response.setMonDefensive(pokemon.getMonDefensive());
        response.setMonSpeed(pokemon.getMonSpeed());

        // response 값 주기
        return response;

    }
}
