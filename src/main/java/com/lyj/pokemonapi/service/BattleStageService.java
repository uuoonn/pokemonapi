package com.lyj.pokemonapi.service;

import com.lyj.pokemonapi.entity.BattleStage;
import com.lyj.pokemonapi.entity.Pokemon;
import com.lyj.pokemonapi.model.battle.MonsterInBattleItem;
import com.lyj.pokemonapi.model.battle.MonsterInBattleResponse;
import com.lyj.pokemonapi.repository.BattleStageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor

public class BattleStageService {

    private final BattleStageRepository battleStageRepository;

    // C 구현하기
    public void setBattle(Pokemon pokemon) throws Exception {
        // 몬스터 둘 이상 결투장 진입이 불가능하니까 현재 몇놈의 몬스터가 결투장에 있는 지 알아야한다.
        List<BattleStage> checkList = battleStageRepository.findAll();
        //만약 checkList의 갯수가 2명 이상이면 던지자.
        if (checkList.size() >= 2) throw new Exception();

        BattleStage battleStage = new BattleStage();
        battleStage.setMonster(pokemon);
        battleStageRepository.save(battleStage);

    }

    // D 구현하기
    public void delStageInMonster(long id) {
        battleStageRepository.deleteById(id);
    }

    // R 구현하기. 배틀 스테이지의 현재 상태를 보여주세요
    public MonsterInBattleResponse getCurrentState() {
        //일단 결투장에 진입한 몬스터 리스트를 다 가져와야한다.
        //근데 넣었을 때 최대 2마리만 넣을 수 있게 해놨으니까
        //경우의 수는 0개, 1개, 2개
        List<BattleStage> checkList = battleStageRepository.findAll();

        //MonsterInBattleResponse의 모양으로 무조건 줘야한다.

        MonsterInBattleResponse response = new MonsterInBattleResponse();
        //MonsterInBattleRespense의 크기는 2이다. 그러므로 선언해주면 두칸 다 null이다.
        //두칸 다 null 이란 건 0개일 때 이미 처리하고 간다는 것이다.

        if (checkList.size() == 2) {
            // 리스트에서 0번째 요소를 가지고 온다.
            response.setMonster1(convertMonsterItem(checkList.get(0)));
            response.setMonster1(convertMonsterItem(checkList.get(1)));

        } else if (checkList.size() == 1) {
            response.setMonster1(convertMonsterItem(checkList.get(0)));
        }

        return response;
    }

    private MonsterInBattleItem convertMonsterItem(BattleStage battleStage) {
        MonsterInBattleItem monsterItem = new MonsterInBattleItem();
        monsterItem.setId(battleStage.getId());
        monsterItem.setMonsterId(battleStage.getMonster().getId());
        monsterItem.setName(battleStage.getMonster().getName());
        monsterItem.setImageUrl(battleStage.getMonster().getImageUrl());
        monsterItem.setMonHp(battleStage.getMonster().getMonHp());
        monsterItem.setMonPower(battleStage.getMonster().getMonPower());
        monsterItem.setMonDefensive(battleStage.getMonster().getMonDefensive());
        monsterItem.setMonSpeed(battleStage.getMonster().getMonSpeed());

        return monsterItem;

    }



}
